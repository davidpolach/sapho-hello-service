package com.sapho.services.sample;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import com.sapho.services.spi.ConfigurationParameter;
import com.sapho.services.spi.ExecutionContext;
import com.sapho.services.spi.action.ServiceAction;
import com.sapho.services.spi.action.ServiceActionMetadata;
import com.sapho.services.spi.data.ExecutionResult;
import com.sapho.services.spi.data.impl.BasicExecutionResult;
import com.sapho.services.spi.data.impl.BasicStateMutator;
import com.sapho.services.spi.data.impl.EntityDataWrapper;

import static com.sapho.services.sample.HelloService.GREETING_ATTRIBUTE;
import static com.sapho.services.sample.HelloService.GREETING_ENTITY;
import static com.sapho.services.sample.HelloService.YOUR_NAME_PARAM_ID;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;

class DefaultAction implements ServiceAction {

    @Override
    public ServiceActionMetadata metadata() {
        return new ServiceActionMetadata() {
            @Override
            public String name() {
                return "Default Hello Download";
            }

            @Override
            public String description() {
                return "";
            }
        };
    }

    @Override
    public Collection<ConfigurationParameter> parameters() {
        return emptySet();
    }

    /**
     * This is the {@link com.sapho.services.spi.Service} body - method called by Sapho server.
     */
    @Override
    public ExecutionResult execute(ExecutionContext context) {
        final EntityDataWrapper entity = new EntityDataWrapper(GREETING_ENTITY);
        entity.set(GREETING_ATTRIBUTE, "Hello " + context.configuration().get(YOUR_NAME_PARAM_ID));

        final BasicStateMutator insertOne = BasicStateMutator.forInsert(
                GREETING_ENTITY,
                singleton(entity.toEntityData()));

        return BasicExecutionResult.instance(
                Arrays.asList(
                        BasicStateMutator.forDeleteAll(GREETING_ENTITY),
                        insertOne),
                Collections.emptySet());
    }

    @Override
    public ExecutionResult demo(ExecutionContext executionContext) {
        return execute(executionContext);
    }

    @Override
    public boolean isDefault() {
        return true;
    }
}
