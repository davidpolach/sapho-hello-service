package com.sapho.services.sample;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nonnull;

import com.sapho.services.sample.update.Update010to020;
import com.sapho.services.spi.ConfigurationParameter;
import com.sapho.services.spi.ExecutionContext;
import com.sapho.services.spi.ServiceMetadata;
import com.sapho.services.spi.action.ServiceAction;
import com.sapho.services.spi.impl.AbstractService;
import com.sapho.services.spi.impl.BasicConfigurationParameter;
import com.sapho.services.spi.impl.BasicServiceMetadata;
import com.sapho.services.spi.model.Attribute;
import com.sapho.services.spi.model.Entity;
import com.sapho.services.spi.update.ServiceUpdate;

import static com.sapho.data.type.BaseDataType.STRING;
import static com.sapho.data.type.TypeMetadata.integer;
import static com.sapho.data.type.TypeMetadata.unlimitedText;
import static com.sapho.services.spi.ParameterRole.PASSWORD;
import static com.sapho.services.spi.model.impl.BasicAttribute.attribute;
import static com.sapho.services.spi.model.impl.BasicEntity.entity;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

/**
 * Sample implementation of {@link com.sapho.services.spi.Service} returning 'Hello [User name]' message.
 * <p/>
 * When configuring new service / data source in Sapho, user sets one configuration parameter - her name. When
 * this service is executed it returns one 'Greeting' entity with 'Hello [User name]' as its attribute value.
 * 'Greeting' entity is stored in Sapho and can be displayed in mobile app.
 */
public class HelloService extends AbstractService {
    /**
     * Service has one configuration parameter - user's name. This is the parameter Id.
     */
    static final String YOUR_NAME_PARAM_ID = "yourName";

    /**
     * Defines 'Greeting' entity attribute.
     */
    static final Attribute GREETING_ATTRIBUTE = attribute("Greeting Text", unlimitedText());

    /**
     * Defines 'Greeting' entity.
     */
    static final Entity GREETING_ENTITY =
            entity("Greeting", "Sample entity with 'hello' message").withAttributes(GREETING_ATTRIBUTE);


    // multiple references test cases
    static final Entity DUMMY_ENTITY = entity("Dummy", "").withAttributes(
            attribute("left", integer()),
            attribute("right", integer()));

    static final Entity DUMMY_2_ENTITY = entity("Dummy2", "").withAttributes(
            attribute("left1", integer()),
            attribute("right1", integer()),
            attribute("left2", integer()),
            attribute("right2", integer()));

    static final Entity SPRINT_ENTITY = entity("Sprint", "").withAttributes(attribute("Id", integer()));

    static final Entity USER_ENTITY = entity("User", "").withAttributes(attribute("Id", integer()));

    static final Entity TICKET_ENTITY =
            entity("Ticket", "").withAttributes(
                    attribute("Name", unlimitedText()),

                    // single attribute ref - no alias
                    attribute("Sprint Id", integer()).withEntityReference("Sprint", "Id"),

                    // single attribute ref - with alias
                    attribute("Single Ref Alias", unlimitedText())
                            .withGroupEntityReference("Greeting", "Greeting Text", "greeting"),

                    // single attribute refs, multi-join aliased
                    attribute("Created By", integer()).withGroupEntityReference("User", "Id", "created by"),
                    attribute("Updated By", integer()).withGroupEntityReference("User", "Id", "updated by"),

                    // multi attribute ref, non-multi-join
                    attribute("Dummy left", integer()).withGroupEntityReference("Dummy", "left", "dummy"),
                    attribute("Dummy right", integer()).withGroupEntityReference("Dummy", "right", "dummy"),

                    // multi attribute ref, multi-join
                    attribute("Dummy2 left1", integer()).withGroupEntityReference("Dummy2", "left1", "dummy2 1"),
                    attribute("Dummy2 right1", integer()).withGroupEntityReference("Dummy2", "right1", "dummy2 1"),

                    attribute("Dummy2 left2", integer()).withGroupEntityReference("Dummy2", "left2", "dummy2 2"),
                    attribute("Dummy2 right2", integer()).withGroupEntityReference("Dummy2", "right2", "dummy2 2"));

    /**
     * Service configuration parameters - there is only one mandatory parameter (already described above).
     */
    private static ConfigurationParameter YOUR_NAME_PARAM =
            new BasicConfigurationParameter(YOUR_NAME_PARAM_ID, "Enter Your Name", STRING, null, true);
    public static ConfigurationParameter SOME_TEXT_PARAM =
            new BasicConfigurationParameter("someText", "Some Text", "", STRING, PASSWORD, null, false);

    private static final List<ConfigurationParameter> PARAMETERS = asList(
            YOUR_NAME_PARAM,
            SOME_TEXT_PARAM);

    @Override
    public ServiceMetadata getMetadata() {
        return new BasicServiceMetadata("Hello Service", "0.2.0");
    }

    @Override
    public List<Entity> getEntities(ExecutionContext executionContext) {
        return Arrays.asList(
                GREETING_ENTITY,
                SPRINT_ENTITY,
                USER_ENTITY,
                TICKET_ENTITY,
                DUMMY_ENTITY,
                DUMMY_2_ENTITY);
    }

    @Override
    public List<ConfigurationParameter> getParameters() {
        return PARAMETERS;
    }

    @Override
    @Nonnull
    public List<ServiceUpdate> getUpdates() {
        return singletonList(new Update010to020().build());
    }

    @Override
    public Collection<ServiceAction> getActions() {
        return Collections.singleton(new DefaultAction());
    }


}
