package com.sapho.services.sample.update;

import com.sapho.services.sample.HelloService;
import com.sapho.services.spi.impl.BasicConfigurationParameter;
import com.sapho.services.spi.update.UpdateType;
import com.sapho.services.spi.update.impl.BasicParameterUpdate;
import com.sapho.services.spi.update.impl.ServiceUpdateBuilder;

import static com.sapho.data.type.BaseDataType.STRING;

public class Update010to020 extends ServiceUpdateBuilder {

    public Update010to020() {
        super("0.1.0", "0.2.0");

        update(new BasicParameterUpdate(UpdateType.MODIFY,
                HelloService.SOME_TEXT_PARAM,
                new BasicConfigurationParameter("someText", "Some Text", STRING, null, false)));
    }
}
